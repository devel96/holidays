<!DOCTYPE html>
<html>
<head>
    <title>Datepicker</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
</head>

<body>

<div class="container">
    <h1>Datepicker</h1>
    <input class="date form-control" type="text">
</div>

<script type="text/javascript">
    $('.date').datepicker({
        format: 'dd.mm.yyyy',
        autoclose: true
    }).on('changeDate', function () {
        $.get("/check-date?date=" + $(this).val(), function(data){
            alert("Data: " + data);
        });
    });
</script>

</body>

</html>
