<?php


namespace App\Strategy;

/**
 * Class FlexibleDayWithWeekDataAndMonthStrategy
 * @package App\Strategy
 */
class FlexibleDayWithWeekDataAndMonthStrategy extends HolidayStrategy
{
    private const MAX_WEEK_COUNT_IN_YEAR = 53;
    private const WEEK_NUMBER_LAST = 'l';
    private const WEEK_NUMBER_FIRST = 'f';

    /**
     * @return array
     * @throws \Exception
     */
    public function getDates(): array
    {
        $month = $this->getMonth();
        $weekNumber = $this->getWeekNumber();
        $isWeekNumeric = true;

        if (!is_numeric($weekNumber)) {
            $weekNumber = $this->getNotFixedWeekNumber($weekNumber);
            $isWeekNumeric = false;
        }

        $this->setHolyDayRealDayByData($month, $weekNumber, $isWeekNumeric);

        $date = [
            $this->getHolidayFullDate($this->holiday->getDay())
        ];

        if ($newDate = $this->addMonday()) {
            $date[] = $newDate;
        }

        return $date;
    }

    /**
     * Должен возврашать 4 или 5
     *
     * @param string $weekNumberStr
     * @return int
     * @throws \Exception
     */
    private function getNotFixedWeekNumber(string $weekNumberStr): int
    {
        if ($weekNumberStr === self::WEEK_NUMBER_FIRST) {
            return $this->getFirstWeekNumber();
        }

        return $this->getLastWeekNumber();
    }

    /**
     * @return int
     * @throws \Exception
     */
    private function getLastWeekNumber(): int
    {
        $month = $this->getMonth(); // Можно было создать аттрибут класса private $month, так было бы правильнее ))
        $lastDate = new \DateTime("2021-$month-01");
        $lastDay = $lastDate->format('t');
        $date = new \DateTime("2021-$month-$lastDay");

        return $date->format('W');
    }

    /**
     * @return int
     * @throws \Exception
     */
    private function getFirstWeekNumber(): int
    {
        $month = $this->getMonth(); // Можно было создать аттрибут класса private $month, так было бы правильнее ))
        $date = new \DateTime("2021-$month-01");

        return $date->format('W');
    }

    /**
     * @param string $week
     * @param string $year
     * @return mixed
     * @throws \Exception
     */
    private function getStartAndEndDate(string $week, string $year)
    {
        $dateTime = new \DateTime();
        $dateTime->setISODate($year, $week);
        $result['start_date'] = $dateTime->format('d');
        $dateTime->modify('+6 days');
        $result['end_date'] = $dateTime->format('d');

        return $result;
    }

    /**
     * @param string $month
     * @param int $weekNumber
     * @param bool $isWeekNameNumeric
     * @throws \Exception
     */
    private function setHolyDayRealDayByData(string $month, int $weekNumber, bool $isWeekNameNumeric = false) // Извиняюсь за флаг )
    {
        if($isWeekNameNumeric) {
            $date = new \DateTime("2021-$month-01");
            $firstWeekOfMonthNumberInYear = $date->format('W');
            $currWeek = $weekNumber + intval($firstWeekOfMonthNumberInYear) - 1;

            if ($currWeek > self::MAX_WEEK_COUNT_IN_YEAR) {
                $currWeek = $currWeek - self::MAX_WEEK_COUNT_IN_YEAR;
            }
        } else {
            $currWeek = $weekNumber;
        }

        $holyDay = $this->getWeekNames()[$this->getWeekDayName()];
        $curWeekStartDate = $this->getStartAndEndDate($currWeek, date('Y'))['start_date'];
        $day = $holyDay + $curWeekStartDate;

        if($day < 10) {
            $day = 0 . $day;
        }

        $this->holiday->setDay($day . '.' . $month);
    }

    /**
     * @return string
     */
    private function getWeekDayName(): string
    {
        return explode(' of the ', $this->holiday->getName())[0];
    }

    /**
     * Могут быть следующие значения
     * 1,2,3,4,5,l(ast),f(irst)
     *
     * @return int|string
     */
    private function getWeekNumber()
    {
        return explode(' of the ', $this->holiday->getName())[1][0];
    }

    /**
     * @return string
     * @throws \Exception
     */
    private function getMonth(): string
    {
        $arr = explode(' ', $this->holiday->getName());
        $monthName = $arr[count($arr) - 1];

        if (!in_array($monthName, $this->getMonthNames())) {
            throw new \Exception('Invalid Month Name');
        }

        $monthNum = date_parse($monthName)['month'];

        return $monthNum >= 10 ? $monthNum : 0 . $monthNum;
    }

    /**
     * @return array
     */
    private function getMonthNames(): array
    {
        return [
            'January', 'February', 'March', 'April',
            'May', 'June', 'July', 'August', 'September',
            'October', 'November', 'December'
        ];
    }

    /**
     * @return array
     */
    private function getWeekNames(): array
    {
        return [
            'Monday' => 0,
            'Tuesday' => 1,
            'Wednesday' => 2,
            'Thursday' => 3,
            'Friday' => 4,
            'Saturday' => 5,
            'Sunday' => 6,
        ];
    }
}
