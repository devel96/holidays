<?php


namespace App\Strategy;


use App\Models\HolidayObject;

abstract class HolidayStrategy implements HolidayStrategyInterface
{
    protected HolidayObject $holiday;

    /**
     * FixedDayStrategy constructor.
     * @param HolidayObject $holiday
     */
    public function __construct(HolidayObject $holiday)
    {
        $this->holiday = $holiday;
    }

    /**
     * @param string $day
     * @return string
     */
    protected function getHolidayFullDate(string $day): string
    {
        return $day . '.' . date('Y');
    }

    /**
     * @return string
     */
    protected function getMondayAfterSaturdayFullDate(): string
    {
        return date('d.m.Y',
        strtotime($this->getHolidayFullDate($this->holiday->getDay()) . ' +2 day'));
    }

    /**
     * @return string
     */
    protected function getMondayAfterSundayFullDate(): string
    {
        return date('d.m.Y',
            strtotime($this->getHolidayFullDate($this->holiday->getDay()) . ' +1 day'));
    }

    /**
     * @return string
     * @throws \Exception
     */
    protected function getWeekDay(): string
    {
        $date = new \DateTime($this->getHolidayFullDate($this->holiday->getDay()));

        return $date->format("D");
    }

    /**
     * @throws \Exception
     */
    protected function addMonday(): ?string
    {
        $weekDay = $this->getWeekDay();

        if ($weekDay === 'Sun') {
            return $this->getMondayAfterSundayFullDate();
        } elseif ($weekDay === 'Sat') {
            return  $this->getMondayAfterSaturdayFullDate();
        }

        return null;
    }

}
