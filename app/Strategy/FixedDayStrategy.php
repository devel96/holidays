<?php


namespace App\Strategy;


use App\Services\HolidayService;

/**
 * Class FixedDayStrategy
 * @package App\Strategy
 */
class FixedDayStrategy extends HolidayStrategy
{
    /**
     * @return array
     * @throws \Exception
     */
    public function getDates(): array
    {
        $date = [
            $this->getHolidayFullDate($this->holiday->getDay())
        ];

        if($newDate = $this->addMonday())
        {
            $date[] = $newDate;
        }

        return $date;
    }
}
