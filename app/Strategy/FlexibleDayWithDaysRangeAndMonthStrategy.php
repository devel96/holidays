<?php


namespace App\Strategy;

/**
 * Class FlexibleDayWithDaysRangeAndMonthStrategy
 * @package App\Strategy
 */
class FlexibleDayWithDaysRangeAndMonthStrategy extends HolidayStrategy
{
    /**
     * @return array
     */
    public function getDates(): array
    {
        $dates = [];
        $daysList = $this->getDaysList();
        $month = $this->getMonth();

        foreach ($daysList as $day) {
            if ($day < 10) {
                $day = '0' . $day;
            }

            $date = $day . '.' . $month;
            $dates[] = $this->getHolidayFullDate($date);
        }

        return $dates;
    }

    /**
     * @return string
     */
    private function getMonth(): string
    {
        return explode('.', $this->holiday->getDay())[1];
    }

    /**
     * @return array
     */
    private function getDaysList(): array
    {
        $daysString = explode('.', $this->holiday->getDay())[0];
        $days = explode('-', $daysString);

        return range($days[0], $days[1]);
    }
}
