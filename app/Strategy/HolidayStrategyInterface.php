<?php


namespace App\Strategy;


interface HolidayStrategyInterface
{
    public function getDates(): array;
}
