<?php


namespace App\Http\Controllers;


use App\Http\Requests\DateRequest;
use App\Services\HolidayService;

class HolidayController extends Controller
{
    public function index()
    {
        return view('welcome');
    }

    public function checkDate(DateRequest $request, HolidayService $holidayService)
    {
        return $holidayService->setDay($request->get('date'))->checkDate();
    }
}
