<?php


namespace App\Services;


use App\Models\Holiday;
use App\Models\HolidayObject;
use App\Strategy\FixedDayStrategy;
use App\Strategy\FlexibleDayWithDaysRangeAndMonthStrategy;
use App\Strategy\FlexibleDayWithWeekDataAndMonthStrategy;
use App\Strategy\HolidayStrategyInterface;

/**
 * Class HolidayService
 * @package App\Services
 */
class HolidayService
{
    private Holiday $holiday;
    private string $day = '';

    /**
     * HolidayService constructor.
     */
    public function __construct()
    {
        $this->holiday = new Holiday();
    }

    /**
     * @param string $day
     * @return $this
     */
    public function setDay(string $day): self
    {
        $this->day = $day;

        return $this;
    }

    /**
     * @return string
     */
    public function checkDate(): string
    {
        $holidays = $this->holiday->getAll();

        $holidayDatesList = [];
        foreach ($holidays as $holiday) {
            $strategyName = $this->getStrategyName($holiday);

            /** @var HolidayStrategyInterface $holidayStrategy */
            $holidayStrategy = new $strategyName($holiday);

            $holidayDatesList = array_merge($holidayDatesList, $holidayStrategy->getDates());
        }

        return in_array($this->day, $holidayDatesList) ? 'Отдых' : 'Рабочий день(либо воскресенье)';
    }

    /**
     * @param HolidayObject $holiday
     * @return string
     */
    private function getStrategyName(HolidayObject $holiday): string
    {
        if ($holiday->getType() === Holiday::TYPE_FIXED) {
            return FixedDayStrategy::class;
        } elseif (strpos($holiday->getName(), ' of the ')) {
            return FlexibleDayWithWeekDataAndMonthStrategy::class;
        } else { // Посколько у нас только этот вариант остается я выбрал else
            return FlexibleDayWithDaysRangeAndMonthStrategy::class;
        }
    }
}
