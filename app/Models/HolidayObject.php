<?php


namespace App\Models;

/**
 * Value object class for holiday
 *
 * Class HolidayObject
 * @package App\Models
 */
class HolidayObject
{
    private string $type;
    private string $day;
    private string $name;

    /**
     * HolidayObject constructor.
     * @param string $type
     * @param string $name
     * @param string $day
     */
    public function __construct(string $type, string $name, string $day = '')
    {
        $this->type = $type;
        $this->name = $name;
        $this->day = $day;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDay(): string
    {
        return $this->day;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param string $day
     * @return $this
     */
    public function setDay(string $day): self
    {
        $this->day = $day;

        return $this;
    }
}
