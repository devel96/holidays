<?php


namespace App\Models;

/**
 * Class Holiday
 * @package App\Models
 */
class Holiday
{
    public const TYPE_FIXED = 'fixedDay';
    public const TYPE_FLEXIBLE = 'flexibleDay';

    /**
     * Классицирование дней
     * Каждый раз когда добавим новый день,
     * надо соответствовать сушествующим классам, либо добавить новый класс(если понадобится)
     *
     * Сейчас мы имеем
     * 1) с фиксированной датой // FixedDayStrategy
     * 2) с не фиксированной датой, день неделей, номером недели и с месяцом // FlexibleDayWithWeekDataAndMonthStrategy
     * 3) с не фиксированной датой, с промежутком дней и с месяцом // FlexibleDayWithDaysRangeAndMonthStrategy
     *
     * @return array
     */
    public function getAll(): array
    {
        return [
            new HolidayObject(self::TYPE_FIXED, '1st of January', '01.01'),
            new HolidayObject(self::TYPE_FIXED, '7th of January', '07.01'),
            new HolidayObject(self::TYPE_FLEXIBLE, 'From 1st of May till 7th of May', '1-7.05'),
            new HolidayObject(self::TYPE_FLEXIBLE, 'Monday of the 3rd week of January'),
            new HolidayObject(self::TYPE_FLEXIBLE, 'Monday of the last week of March'),
            new HolidayObject(self::TYPE_FLEXIBLE, 'Thursday of the 4th week of November'),
        ];
    }
}
